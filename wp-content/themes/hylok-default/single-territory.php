<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<!--Site Content-->
<section class="site-intro">
    <div class="inner-wrap">
     		<h1 class="page-intro-header"><?php the_title(); ?></h1>
        <div class="site-intro-body">
        </div>
    </div>
</section>


	<section class="site-content" role="main">
	    <div class="inner-wrap">
	    	
	        <article class="site-content-primary col-8">
				<h2><?php the_sub_field('company_name'); ?></h2>	

						<h3>Direct Contacts</h3>

						<p>
						<?php if( have_rows('ter_contact') ): while( have_rows('ter_contact') ): the_row(); ?>						
							<?php the_sub_field('contact_name'); ?>: <a href="mailto:<?php the_sub_field('contact_email'); ?>"> <?php the_sub_field('contact_email'); ?></a><br>
						<?php endwhile; endif; ?>
						</p>
						

						<h3>Address</h3>
						<p><?php the_field('address_1'); ?><br>

						<?php if (the_field('address_2')): ?>
							<?php the_field('address_2'); ?><br>
						<?php endif ?>

						<?php the_field('ter_city'); ?>, <?php the_field('ter_state'); ?>, <?php the_field('company_zip_code'); ?><br>
						<?php the_field('ter_country'); ?><br>
						
						
						<h3>Additional Contact Info</h3>
						<p>Phone: <?php the_field('ter_phone'); ?> <br>
						Fax: <?php the_field('ter_fax'); ?></p>
							
						<?php the_excerpt(); ?>
	        </article>
	       	
	    </div>
	</section>
<?php endwhile; ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/resources-module','parts/shared/distributor-locator-module','parts/shared/footer','parts/shared/html-footer' ) ); ?>

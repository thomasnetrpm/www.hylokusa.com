<?php
/**
 * Search results page modified specifically for the locator distributor
 * 
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<section class="site-content" role="main">
		<section class="site-intro">
		    <div class="inner-wrap">
		        <h1 class="page-intro-header">The Distributor for Zip Code '<?php echo get_search_query(); ?>'</h1>
		    </div>
		</section>
    <div class="inner-wrap">
        <article class="site-content-primary">   
			<?php if ( have_posts() ): ?>    							

<!-- 				<h4 style="text-align:right;color:#999">These are the Search Results for Zip Code '<?php echo get_search_query(); ?>'</h4>
 -->
				<?php while ( have_posts() ) : the_post(); ?>
					<article>

						<div class="col-6 dl-results-left">
						<h1><?php the_title(); ?></h1>
						<h2><?php the_sub_field('company_name'); ?></h2>	

						<h3>Direct Contacts</h3>

						<p>
						<?php if( have_rows('ter_contact') ): while( have_rows('ter_contact') ): the_row(); ?>	
							<?php if (get_sub_field('contact_name')): ?>					
									<?php the_sub_field('contact_name'); ?>:
							<?php endif ?>
						 	<a href="mailto:<?php the_sub_field('contact_email'); ?>"> <?php the_sub_field('contact_email'); ?></a><br>
						<?php endwhile; endif; ?>
						</p>
						

						<h3>Address</h3>
						<p><?php the_field('address_1'); ?><br>

						<?php if (get_field('address_2')): ?>
							<?php the_field('address_2'); ?><br>
						<?php endif ?>

						<?php the_field('ter_city'); ?>, <?php the_field('ter_state'); ?>, <?php the_field('company_zip_code'); ?><br>
						<?php the_field('ter_country'); ?><br>
						

						<?php if (get_field('ter_phone') || get_field('ter_fax')): ?>
							<h3>Additional Contact Info</h3>
						<?php endif ?>
						<p>
						<?php if (get_field('ter_phone')): ?>
							Phone: <?php the_field('ter_phone'); ?> <br>
						<?php endif ?>

						<?php if (get_field('ter_fax')): ?>
							Fax: <?php the_field('ter_fax'); ?>
						<?php endif ?>

						</p>
							</p>
							<?php the_excerpt(); ?>
					<div class="col-6 col-last dl-results-right"> 

						<?php if (get_field('ter_contact_form')): ?>
							<?php the_field('ter_contact_form'); ?>
						<?php endif ?></div>
					</article>
				<?php endwhile; ?>
				<?php else: ?>
				
						<div class="col-6 dl-results-left">
							<h2>Hy-lok USA</h2>	
							<p>For direct contact, please complete the form to the right</p>
<!-- 
						<h3>Direct Contacts</h3>

						<p>				
							Hy-lok USA: <a href="mailto:info@hylokusa.com"> info@hylokusa.com</a><br>
						</p>
						 -->

						<h3>Address</h3>
						<p>14211 Westfair West Drive<br>

						Houston, TX, 77041<br>
						United States<br>
						
						
						<h3>Additional Contact Info</h3>
						<p>Phone: 888.300.5708  <br></p>
						<?php if (get_field('ter_contact_form')): ?>
							<?php the_field('ter_contact_form'); ?>
						<?php endif ?>
						</div>
							<div class="col-6 col-last dl-results-right"> <script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>
    <script>
    hbspt.forms.create({
    portalId: '376710',
    formId: '470b25fb-0657-42de-a8d6-4bbcfc4ed2e1',
    css: '.hs-form-field {padding-right:1em;}'
    });
    </script></div>
			<?php endif; ?>
			<?php wp_pagenavi(); ?>
		</article>
	</div>
</section>
<hr>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/resources-module','parts/shared/distributor-locator-module','parts/shared/footer','parts/shared/html-footer' ) ); ?>

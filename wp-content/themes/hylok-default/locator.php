<?php
/*
Template Name:Locator
*/
?>
 
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-intro' ) ); ?>

      <section class="site-content-container">
	    <div class="inner-wrap">

			<div class="site-content-primary fullwidth">
			<?php if(get_field('aside_cta') ): ?>
			<div class="cta-aside alignright">
			<?php the_field('aside_cta'); ?>
			</div>                 
			<?php elseif(get_field('global_aside_cta','option') ): ?>
			<div class="cta-aside">
			<?php the_field('global_aside_cta','option'); ?>
			</div>
			<?php endif; ?>

					<p class="emph">Hy-Lok USA, Inc. is the American master distributor of the Hy-Lok Corporation, an international manufacturer instrumentation valves and fittings.</p>
					<div class="row">
						<div class="col-7">
						
							<p>We are proud to offer our customers a comprehensive line of instrumentation valve and fittings solutions for every project, no matter the regulation or specification. Our dedication and commitment to providing our customers with high quality products is seen through our certifications in a variety of industry requirements, including ISO, BSI, and shipping class type approval.</p> <p>The Hy-Lok Corporation's capabilities include serving varied industries such as chemical processing, oil and gas refining, pharmaceutical and biotechnology, alternative fuels, aerospace, and defense.</p>
							
							<p>No matter the industry, no matter the specifications, Hy-Lok USA can offer you solutions completely tailored to your project. Contact a distributor near you today.</p>
							<hr>
							<div class="row">
								<div class="col-6">
								<h3>Hy-Lok Korea Main Headquarters</h3>
								<p> 97, Noksansandn 27-ro Ganseo-gu, Busan
								<br>TEL: 051-9700-800<br>FAX: 051-831-7921</p>
								</div>
								
								<div class="col-6">
								<h3>Hy-Lok USA</h3>
								<p>14211 Westfair West Dr., Houston, TX 77041, USA
								<br>TEL: 832.634.2000<br>FAX: 832.634.2099</p>	
								</div>
							</div>
						
						</div>	

						<div class="col-5 emph-wrap distributor-locator-forms">
						<p><strong>Please enter your zip code or country to find the nearest Hy-Lok USA, Inc. distributor.</strong></p>

							<form action="<?php bloginfo('url'); ?>/" method="get" class="search-form">

								<h3>Locate US Distributor</h3>
								<p class="ico-us-flag clearfix"><img src="<?php bloginfo('url'); ?>/images/americanflag-hy-lok.png" class="alignleft"></p>

								<h4>Enter Zip code:</h4>
								<input type="text" id="search-site" value="" placeholder="Zip Code" name="s"  size="5" maxlength="5" title="Search Our Site">

								<input type="submit" value="Search">

							</form>

							<form action="<?php bloginfo('url'); ?>/" method="get" class="search-form">

								<h3>Locate International Distributor</h3>

								<p class="ico-international-flags"><img src="<?php bloginfo('url'); ?>/images/internationalflags-hy-lok.png" class="alignleft"></p>

								<h4>Select Country:</h4>
								<select id="search-site" value="" name="s" ><option value="Africa">Africa</option><option value="Albania">Albania</option><option value="Algeria">Algeria</option><option value="Angola">Angola</option><option value="Argentina">Argentina</option><option value="Armenia">Armenia</option><option value="Australia">Australia</option><option value="Austria">Austria</option><option value="Azerbaijan">Azerbaijan</option><option value="Bahrain">Bahrain</option><option value="Bangladesh">Bangladesh</option><option value="Belarus">Belarus</option><option value="Belgium">Belgium</option><option value="Benin">Benin</option><option value="Bhutan">Bhutan</option><option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option><option value="Botswana">Botswana</option><option value="Brazil">Brazil</option><option value="Bulgaria">Bulgaria</option><option value="Burkina Faso">Burkina Faso</option><option value="Burundi">Burundi</option><option value="Cameroon">Cameroon</option><option value="Canada">Canada</option><option value="Cape Verde">Cape Verde</option><option value="Central African Republic">Central African Republic</option><option value="Chad">Chad</option><option value="Channel Islands">Channel Islands</option><option value="Chile">Chile</option><option value="China">China</option><option value="Colombia">Colombia</option><option value="Comoros">Comoros</option><option value="Congo">Congo</option><option value="Costa Rica">Costa Rica</option><option value="C�te d'Ivoire">Côte d'Ivoire</option><option value="Countries">Countries</option><option value="Croatia">Croatia</option><option value="Cyprus">Cyprus</option><option value="Czech Republic">Czech Republic</option><option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option><option value="Denmark">Denmark</option><option value="Djibouti">Djibouti</option><option value="Ecuador">Ecuador</option><option value="Egypt">Egypt</option><option value="El Salvador">El Salvador</option><option value="Equatorial Guinea">Equatorial Guinea</option><option value="Eritrea">Eritrea</option><option value="Estonia">Estonia</option><option value="Ethiopia">Ethiopia</option><option value="Fiji">Fiji</option><option value="Finland">Finland</option><option value="France">France</option><option value="French Polynesia">French Polynesia</option><option value="Gabon Sao Tome and Principe">Gabon Sao Tome and Principe</option><option value="Gambia">Gambia</option><option value="Georgia">Georgia</option><option value="Germany">Germany</option><option value="Ghana">Ghana</option><option value="Greece">Greece</option><option value="Guam">Guam</option><option value="Guatemala">Guatemala</option><option value="Guinea">Guinea</option><option value="Guinea-Bissau">Guinea-Bissau</option><option value="Honduras">Honduras</option><option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option><option value="Iceland">Iceland</option><option value="India">India</option><option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option><option value="Ireland">Ireland</option><option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Jordan">Jordan</option><option value="Kazakhstan">Kazakhstan</option><option value="Kenya">Kenya</option><option value="Kuwait">Kuwait</option><option value="Kyrgyzstan">Kyrgyzstan</option><option value="Latvia">Latvia</option><option value="Lebanon">Lebanon</option><option value="Lesotho">Lesotho</option><option value="Liberia">Liberia</option><option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option><option value="Lithuania">Lithuania</option><option value="Luxembourg">Luxembourg</option><option value="Macao">Macao</option><option value="Madagascar">Madagascar</option><option value="Malawi">Malawi</option><option value="Maldives">Maldives</option><option value="Mali">Mali</option><option value="Malta">Malta</option><option value="Mauritania">Mauritania</option><option value="Mauritius">Mauritius</option><option value="Mayotte">Mayotte</option><option value="Mexico">Mexico</option><option value="Micronesia (Federated States of)">Micronesia (Federated States of)</option><option value="Mongolia">Mongolia</option><option value="Montenegro">Montenegro</option><option value="Morocco">Morocco</option><option value="Mozambique">Mozambique</option><option value="Namibia">Namibia</option><option value="Nepal">Nepal</option><option value="Netherlands">Netherlands</option><option value="New Caledonia">New Caledonia</option><option value="New Zealand">New Zealand</option><option value="Nicaragua">Nicaragua</option><option value="Niger">Niger</option><option value="Nigeria Senegal">Nigeria Senegal</option><option value="Northern Ireland">Northern Ireland</option><option value="Norway">Norway</option><option value="Occupied Palestinian Territory">Occupied Palestinian Territory</option><option value="Oman">Oman</option><option value="Pakistan">Pakistan</option><option value="Panama">Panama</option><option value="Papua New Guinea">Papua New Guinea</option><option value="Peru">Peru</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Puerto Rico">Puerto Rico</option><option value="Qatar">Qatar</option><option value="Republic of Moldova">Republic of Moldova</option><option value="R�union">Réunion</option><option value="Romania">Romania</option><option value="Russian Federation">Russian Federation</option><option value="Rwanda">Rwanda</option><option value="Samoa">Samoa</option><option value="Saudi Arabia Syrian Arab Republic">Saudi Arabia Syrian Arab Republic</option><option value="Serbia">Serbia</option><option value="Sierra Leone">Sierra Leone</option><option value="Slovakia">Slovakia</option><option value="Slovenia">Slovenia</option><option value="Solomon Islands">Solomon Islands</option><option value="Somalia">Somalia</option><option value="South Africa">South Africa</option><option value="Spain">Spain</option><option value="Sri Lanka">Sri Lanka</option><option value="Sudan">Sudan</option><option value="Swaziland">Swaziland</option><option value="Sweden">Sweden</option><option value="Switzerland">Switzerland</option><option value="Tajikistan">Tajikistan</option><option value="The former Yugoslav Republic of Macedonia">The former Yugoslav Republic of Macedonia</option><option value="Togo">Togo</option><option value="Tonga">Tonga</option><option value="Tunisia">Tunisia</option><option value="Turkey">Turkey</option><option value="Turkmenistan">Turkmenistan</option><option value="Uganda">Uganda</option><option value="Ukraine">Ukraine</option><option value="United Arab Emirates">United Arab Emirates</option><option value="United Kingdom">United Kingdom</option><option value="United Republic of Tanzania">United Republic of Tanzania</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option><option value="Venezuela">Venezuela</option><option value="Virgin Islands">Virgin Islands</option><option value="Western Sahara">Western Sahara</option><option value="Yemen">Yemen</option><option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option></select>		     
								<input type="submit" value="Search">

							</form>		
						</div>
					</div><!--end row -->

	       	<?php the_content(); ?> 
					
					<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>                     
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content-fullwidth' ) ); ?>
	    </div>

			

		</div>
	</section>

<?php if(get_field('slide_cta') ): ?>
	 <p id="last"></p>
           <div id="slidebox"><a class="close">&nbsp;</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>
<?php endif; ?>
<hr>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/resources-module','parts/shared/footer','parts/shared/html-footer' ) ); ?>
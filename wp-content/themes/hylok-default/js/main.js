//PNG Fallback

if (!Modernizr.svg) {
  var images = $('img[data-png-fallback]');
  images.each(function(i) {
    $(this).attr('src', $(this).data('png-fallback'));
  });
}

//Toggle Boxes
$(document).ready(function() {
  $('body').addClass('js');
  var $activatelink = $('.activate-link');

  $activatelink.click(function() {
    var $this = $(this);
    $this.toggleClass('active').next('div').toggleClass('active');
    return false;
  });

});

//Responsive Navigation
$(document).ready(function() {
  $('body').addClass('js');
  var $menu = $('#menu'),
    $menulink = $('.menu-link'),
    $menuTrigger = $('.has-subnav > a');
  $searchlink = $('.search-link');

  $menulink.click(function(e) {
    e.preventDefault();
    $menulink.toggleClass('active');
    $menu.toggleClass('active');
  });

  $menuTrigger.click(function(e) {
    e.preventDefault();
    var $this = $(this);
    $this.toggleClass('active').next('ul').toggleClass('active');
  });
  $searchlink.click(function(e) {
    e.preventDefault();
    $('.search-module').toggleClass('active');
  });

});


//Lightbox
$(document).ready(function() {
  $('.lightbox').magnificPopup({ type: 'image' });
});
$(document).ready(function() {
  $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false
  });
});


// Modal Catalog Access LP Form
/*$(document).ready(function() {
  $(".fittings-catalogs-section a").addClass("fittings-catalog-form-popup");
  $(".valves-catalogs-section a").addClass("valves-catalog-form-popup");
  
  window.$('.fittings-catalog-form-popup').magnificPopup({
    items: {
      src: '#fittings-catalog-form-popup',
    },
    type: 'inline',
      removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
      mainClass: 'mfp-fade', //The actual animation
    });
  
  window.$('.valves-catalog-form-popup').magnificPopup({
    items: {
      src: '#valves-catalog-form-popup',  
    },
    type: 'inline',
      removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
      mainClass: 'mfp-fade', //The actual animation
    });
});*/


//Delayed Popup with localstorage to show popup only once
// $(document).ready(function() {
//   if (!localStorage.getItem('popup_show2')) {

//     setTimeout(function() {
//       $.magnificPopup.open({
//         items: {
//           src: '#delayed-popup' //ID of inline element
//         },
//         type: 'inline',
//         removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
//         mainClass: 'mfp-fade', //The actual animation

//       });
//     }, 5000); //Initial popup delay, 5 seconds

//     localStorage.setItem('popup_show2', 'true'); // Set the flag in localStorage
//   }
// });


//Show More
$(document).ready(function() {
  $(".showmore").after("<p><a href='#' class='show-more-link'>More</a></p>");
  var $showmorelink = $('.showmore-link');
  $showmorelink.click(function() {
    var $this = $(this);
    var $showmorecontent = $('.showmore');
    $this.toggleClass('active');
    $showmorecontent.toggleClass('active');
    return false;
  });
});

//Show More
$(document).ready(function() {
  var $expandlink = $('.headexpand');
  $expandlink.click(function() {
    var $this = $(this);
    var $showmorecontent = $('.showmore');


    $this.toggleClass('active').next().toggleClass('active');
    $showmorecontent.toggleClass('active');
    return false;
  });
});


//Flexslider    
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "fade",
    easing: "swing",
    slideshowSpeed: 2800,
    animationDuration: 600,
    animationLoop: true,
    controlNav: false,
    minItems: 1,
    maxItems: 1
  });
});


// $(function () {
//   $('.popup-modal').magnificPopup({
//     type: 'inline',
//     preloader: false,
//     focus: '#username',
//     modal: true
//   });
//   $(document).on('click', '.popup-modal-dismiss', function (e) {
//     e.preventDefault();
//     $.magnificPopup.close();
//   });
// });


//Sticky Nav
$(function() {
  //Set the height of the sticky container to the height of the nav
  //var navheight = $('.site-nav-container').height();
  // grab the initial top offset of the navigation 
  // var sticky_navigation_offset_top = $('.sh-sticky-wrap').offset().top;




  var nav = $('.sh-sticky-wrap');
  if (nav.length) {
    var sticky_navigation_offset_top = nav.offset().top;
    // our function that decides weather the navigation bar should have "fixed" css position or not.
    var sticky_navigation = function() {
      var scroll_top = $(window).scrollTop(); // our current vertical position from the top

      // if we've scrolled more than the navigation, change its position to fixed to stick to top,
      // otherwise change it back to relative
      if (scroll_top > sticky_navigation_offset_top) {
        $('.sh-sticky-wrap').addClass('stuck');
      } else {
        $('.sh-sticky-wrap').removeClass('stuck');
      }
    };

    // run our function on load
    sticky_navigation();

    // and run it again every time you scroll
    $(window).scroll(function() {
      sticky_navigation();
    });
  }




});


//Slide in CTA
// $(function() {
//   var slidebox = $('#slidebox');
//   if (slidebox) {
//     $(window).scroll(function(){
//         var distanceTop = $('#last').offset().top - $(window).height();
//         if  ($(window).scrollTop() > distanceTop)
//             slidebox.animate({'right':'0px'},300);
//         else
//             slidebox.stop(true).animate({'right':'-430px'},100);
//     });
//     $('#slidebox .close').on('click',function(){
//         $(this).parent().remove();
//     });
//   }
// });

$(document).ready(function() {


  $('.nav-7-gdd > a').magnificPopup({
    type: 'ajax'
  });
  // $('.btn').magnificPopup({
  //   items: {
  //     src: '#catalog-form-popup',
  //     type: 'inline',
  //     removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
  //     mainClass: 'mfp-fade', //The actual animation
  //   }
  // });

});

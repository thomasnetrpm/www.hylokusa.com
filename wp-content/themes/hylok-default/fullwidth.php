<?php
/*
Template Name:Fullwidth
*/
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-intro' ) ); ?>
      <section class="site-content-container">
	    <div class="inner-wrap">

	       
	        	
	       	<div class="site-content-primary fullwidth">
<?php if(get_field('aside_cta') ): ?>
<div class="cta-aside alignright">
<?php the_field('aside_cta'); ?>
</div>                 
<?php elseif(get_field('global_aside_cta','option') ): ?>
<div class="cta-aside">
<?php the_field('global_aside_cta','option'); ?>
</div>
<?php endif; ?>
	       		<?php the_content(); ?> 
				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
				<?php if (is_page( '9' )) : ?>
					<!--Sitemap Page-->
				    <ul>
				    <?php
				    // Add pages you'd like to exclude in the exclude here
				    wp_list_pages(
				    array(
				    'exclude' => '',
				    'title_li' => '',
				    )
				    );
				    ?>
				    </ul>
				<?php endif; ?>                 
	       
	        
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content-fullwidth' ) ); ?>
	        </div>

			

		</div>
	</section>

<?php endwhile; ?>

<?php if(get_field('slide_cta') ): ?>
	 <p id="last"></p>
           <div id="slidebox"><a class="close">&nbsp;</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>
<?php endif; ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/resources-module','parts/shared/distributor-locator-module','parts/shared/footer','parts/shared/html-footer' ) ); ?>



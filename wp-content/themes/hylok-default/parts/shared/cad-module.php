<section class="cad-module">
                <div class="inner-wrap">
                    <div class="cad-module-container">
                        <h3 class="cad-module-header"><b>CAD drawings</b> of Valves, Fittings, and High-Purity Components</h3>
                        <a href="/hy-lok-cad-drawings" rel="nofollow" class="btn" target="_blank">Browse and Download</a>
                    </div>
            </section>

<?php if( have_rows('flexible_content') ): echo '<section class="additional-content">';
    while ( have_rows('flexible_content') ) : the_row(); ?>

	<?php if( get_row_layout() == 'spec_table' ): ?>
		<?php if( get_sub_field('fullwidth')): ?>
		 	<article>
				<?php if( get_sub_field('section_header')): ?>
					<h2><?php the_sub_field('section_header'); ?></h2>
				<?php endif; ?>
				<?php if( get_sub_field('section_subtext')): ?>
					<p><?php the_sub_field('section_subtext'); ?></p>
				<?php endif; ?>

				<section class="table colored-rows col-9">
				<?php if( have_rows('spec_table_row') ): 

				while ( have_rows('spec_table_row') ) : the_row(); ?>
				<div class="table-row">
				<div class="cell spec-col-1">
					<p><strong>
					<?php the_sub_field('spec_header'); ?>
					</strong></p>
				</div>
				<div class="cell spec-col-2"><p>
					<?php the_sub_field('spec_body'); ?>
				</p></div>
				</div>
				<?php endwhile; ?>


				<?php endif; ?>
				</section>
			</article>
			<?php if( get_sub_field('divider')): ?>
					<hr>
			<?php endif; ?>
		<?php endif; ?>


	<?php elseif( get_row_layout() == 'full_width_cta' ): ?>
		<?php if( get_sub_field('fullwidth')): ?>
			<div class="row cta-banner bottom-baseline">
	            <span class="col-6of9">
	            <h2 class="cta-banner-header"><?php the_sub_field('section_header'); ?></h2>
	            <p class="cta-banner-body"><?php the_sub_field('section_body'); ?></p>
	            
	        </span>
	        <a href="<?php the_sub_field('url'); ?>" class="red-btn-l col-3of9 col-last cta-download" target="_blank"><?php the_sub_field('cta_button'); ?></a>
	        </div>
	        <?php if( get_sub_field('divider')): ?>
					<hr>
			<?php endif; ?>
		<?php endif; ?>


 	<?php elseif( get_row_layout() == 'multiple_columns' ): ?>
	 	<?php if( get_sub_field('fullwidth')): ?>
	 		<?php if( get_sub_field('section_header')): ?>
				<h2><?php the_sub_field('section_header'); ?></h2>
			<?php endif; ?>
			<?php if( get_sub_field('section_subtext')): ?>
				<p><?php the_sub_field('section_subtext'); ?></p>
			<?php endif; ?>
				<section class="<?php if( get_sub_field('section_classes')): ?><?php the_sub_field('section_classes'); ?><?php endif; ?> <?php if (get_sub_field('number_columns') == '2') {
						echo 'rows-of-2';
					} else if (get_sub_field('number_columns') == '3') {
					        echo 'rows-of-3';
					} else if (get_sub_field('number_columns') == '4') {
					        echo 'rows-of-4';
					}
					?>">

	         	<?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
					<div><?php the_sub_field('content_column'); ?></div>
				<?php endwhile; ?>
				<?php endif; ?>
				</section>
			<?php if( get_sub_field('divider')): ?>
				<hr>
			<?php endif; ?>
		<?php endif; ?>

 			
 			
	<?php elseif( get_row_layout() == 'table' ): ?>
	    <?php if( get_sub_field('fullwidth')): ?>
	        <?php if( get_sub_field('section_header')): ?>
	            <div class="headexpand-wrap">  
	            <h3 class="headexpand"><?php the_sub_field('section_header'); ?></h3>
	        <?php endif; ?>

	        <?php if( get_sub_field('table_content')): ?>
	            <div class="table-wrap">
	                <table class="tablesaw" data-mode="stack">
	                <?php the_sub_field('table_content'); ?>
	                </table>
	            </div>
	        <?php endif; ?>
	        <?php if( get_sub_field('section_header')): ?>
	           </div> <!--headexpand-wrap END -->
	        <?php endif; ?>
	        <?php if( get_sub_field('divider')): ?>
				<hr>
			<?php endif; ?>
	    <?php endif; ?>


	<?php elseif( get_row_layout() == 'picture_grid' ): ?>
		<?php if( get_sub_field('fullwidth')): ?>
				<?php if( get_sub_field('section_header')): ?>
					<h2><?php the_sub_field('section_header'); ?></h2>
				<?php endif; ?>
				<?php if( get_sub_field('section_subtext')): ?>
					<p><?php the_sub_field('section_subtext'); ?></p>
				<?php endif; ?>
		<section class="<?php if (get_sub_field('number_columns') == '2') {
							echo 'rows-of-2';
						} else if (get_sub_field('number_columns') == '3') {
						        echo 'rows-of-3';
						} else if (get_sub_field('number_columns') == '4') {
						        echo 'rows-of-4';
						}
						?>">	
		<?php if( get_sub_field('lightbox')): ?>	
			<?php if( have_rows('picture_repeat') ):		
			while ( have_rows('picture_repeat') ) : the_row(); ?>
						
				<?php 	
					$image = wp_get_attachment_image_src(get_sub_field('picture'), 'thumbnail');
					$imagelarge = wp_get_attachment_image_src(get_sub_field('picture'), 'full');
				 ?>
			 
			
				<a href="<?php echo $imagelarge[0]; ?>" class="lightbox bottom-baseline">
						 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
					<?php if( get_sub_field('sub_title')): ?>
						<figcaption><?php the_sub_field('sub_title'); ?></figcaption>
					<?php endif; ?>
				</a>
				<?php if( get_sub_field('divider')): ?>
		 				<hr>
					<?php endif; ?>
			
			
				
			<?php endwhile; ?>
			<?php endif; ?>
			
		<?php else : ?>
			<?php if( have_rows('picture_repeat') ):		
			while ( have_rows('picture_repeat') ) : the_row(); ?>
						
				<?php 	
					$image = wp_get_attachment_image_src(get_sub_field('picture'), 'thumbnail');
					$imagelarge = wp_get_attachment_image_src(get_sub_field('picture'), 'full');
				 ?>
			 
			
				<figure class="bottom-baseline">
					 <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
				<?php if( get_sub_field('sub_title')): ?>
					<figcaption><?php the_sub_field('sub_title'); ?></figcaption>
				<?php endif; ?>
			</figure>
				
			<?php if( get_sub_field('divider')): ?>
		 				<hr>
					<?php endif; ?>
			
				
			<?php endwhile; ?>
			<?php endif; ?>
			
		<?php endif; ?>
		</section>
		<?php endif; ?>


	<?php elseif( get_row_layout() == 'product_grid' ): ?>
		<?php if( get_sub_field('fullwidth')): ?>
			<?php if( get_sub_field('section_header')): ?>
				<h2 class="carousel-header"><span><?php the_sub_field('section_header'); ?></span></h2>
			<?php endif; ?>
			<?php if( get_sub_field('section_subtext')): ?>
				<p><?php the_sub_field('section_subtext'); ?></p>
			<?php endif; ?>

			<div class="<?php if( get_sub_field('carousel')): ?>flexslider<?php endif; ?> product-carousel">
			<ul class="slides">
				<?php if( have_rows('product_row') ): while ( have_rows('product_row') ) : the_row(); ?>
				<li>
					<?php if( have_rows('product_item') ):		
					while ( have_rows('product_item') ) : the_row(); ?>
					<?php 	
					$image = wp_get_attachment_image_src(get_sub_field('product_picture'), 'thumbnail');
					$imagelarge = wp_get_attachment_image_src(get_sub_field('product_picture'), 'full');
					?>
					<a class="product-item" href="<?php the_sub_field('product_url'); ?>"> 
							<h3 class="product-header"><?php the_sub_field('product_body'); ?></h3> 
							<span class="product-img">
							<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
							</span>
							<span class="product-cta">Learn More</span>
					</a>
					<?php endwhile; ?>
					<?php endif; ?>
				</li>
				<?php endwhile; ?>
				<?php endif; ?>
			</ul>
			</div>

			<?php if( get_sub_field('divider')): ?>
				<hr>
			<?php endif; ?>
		<?php endif; ?>



	<?php elseif( get_row_layout() == 'text_media' ): ?>
		<?php if( get_sub_field('fullwidth')): ?>
			<?php if( get_sub_field('section_subtext')): ?>
				<p><?php the_sub_field('section_subtext'); ?></p>
			<?php endif; ?>
			
	     	<article class="clearfix">
	    		
	    		<div class="col-3of9">
	    			<?php the_sub_field('media'); ?>
	    		</div>
	    		<div class="col-6of9 col-last">
	    		<?php if( get_sub_field('section_header')): ?>
				<h2><?php the_sub_field('section_header'); ?></h2>
			<?php endif; ?>
	    			<?php the_sub_field('text'); ?>
	    		</div>
	    		
				</article>
				<?php if( get_sub_field('divider')): ?>
					<hr>
			<?php endif; ?>
		<?php endif; ?>
	



<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>






<section class="site-intro">
    <div class="inner-wrap">
            <h1 class="page-intro-header"><?php the_title(); ?></h1>
        <div class="site-intro-body">
        </div>
    </div>
</section>
<?php if ( function_exists('yoast_breadcrumb') ) 
{yoast_breadcrumb('<div class="breadcrumb-nav"><div class="inner-wrap"><p id="breadcrumbs">','</p></div></div>');} ?>
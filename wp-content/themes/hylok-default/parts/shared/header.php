<!--Site Header-->
<header class="site-header" role="banner">

    <!--Site Search Module-->
    <div class="search-module">
        <div class="inner-wrap">
            <form action="http://valvesandfittings.hylokusa.com/keyword/?&amp;plpver=1001&amp;key=all&amp;keycateg=100" method="get" class="search-form">
                <div class="search-table">
                  <div class="search-row">
                   <div class="search-cell1">
                      <input type="text" value="" placeholder="Search Website..." name="keyword" id="keyword" class="search-text" title="Search Our Site">
                    </div>
                    <div class="search-cell2">
                      <input type="submit" value="Submit" title="Submit" class="search-submit">
                    </div>           
                  </div>
                </div>
            </form> 
            
          
            
            <a href="#" target="_blank" class="search-link search-exit"><img src="<?php bloginfo('template_url'); ?>/img/ico-exit.svg" data-png-fallback="img/ico-exit.png" alt="Exit"></a>
        </div>
    </div>

    <div class="inner-wrap">
        <a href="/" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/hylok-logo.jpg" alt="Hy-Lok Logo"></a>
        <div class="site-header-wrap">
        <div class="utility-nav">
            <a href="http://www.hylokusa.com/certifications.html" target="_blank" class="ico-iso"><img src="<?php bloginfo('template_url'); ?>/img/ico-iso.svg" alt="Hy-Lok ISO Certification"></a>
            <div class="social-wrap tablet">
            <a href="https://www.facebook.com/HyLokUSA" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="img/ico-facebook.png" alt="Facebook"></a>
            <a href="https://twitter.com/hylokfittings" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="img/ico-twitter.png" alt="Twitter"></a>
            <a href="https://plus.google.com/+Hylokusa/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" data-png-fallback="img/ico-googleplus.png" alt="Google Plus"></a>
            <a href="https://www.linkedin.com/company/hy-lok-usa" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="img/ico-linkedin.png" alt="LinkedIn"></a>
            <a href="http://www.hylokusa.com/blog/"><img src="<?php bloginfo('template_url'); ?>/img/ico-blog.svg" data-png-fallback="img/ico-blog.png" alt="LinkedIn"></a>
            <a href="https://www.youtube.com/channel/UCz2J_DBcmdXVCb1Jytu5IhA/about" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-youtube.svg" data-png-fallback="img/ico-youtube.png" alt="LinkedIn"></a>
            </div>
            <span class="site-ph"><span>888.300.5708</span></span>
            <a href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=info@hylokusa.com" target="_blank" class="site-email">Email</a>
            <a href="http://intranet.hylokusa.com/" target="_blank" class="portal-link">Distributor Portal</a>
        </div>
        <!-- Sticky Nav Wrap -->
     <div class="sh-sticky-wrap">
            <nav id="menu" class="menu clearfix site-nav" role="navigation">
            <ul class="level-1">
                <?php wp_nav_menu(array('menu' => 'Primary Nav','container' => '','items_wrap'      => '%3$s',)); ?> 
                <li class="nav-8 nav-search"><a href="#" class="search-link">Search</a></li>
                </ul>
            </nav>
        </div>
        </div>
        <a href="#menu" class="mobile-nav mobile menu-link active">
                <span>Menu</span>
            </a>
    </div>

                                  
</header>



 






<!--Site Content-->
<section class="site-content" role="main">

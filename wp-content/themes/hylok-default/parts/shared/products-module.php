<section class="products-module">
                 
                    <a href="/products/valves" class="products-module-item-1">
                        <figure class="products-module-img">
                            <img src="<?php bloginfo('template_url'); ?>/img/thumb-valves-new.png" alt="Valves">
                        </figure>
                        <h2 class="products-module-title arrowright-link">Valves</h2>
                    </a>
                    <a href="/products/fittings" class="products-module-item-2">
                        <figure class="products-module-img">
                            <img src="<?php bloginfo('template_url'); ?>/img/thumb-fittings-new.png" alt="Fittings">
                        </figure>
                        <h2 class="products-module-title arrowright-link">Fittings</h2>
                    </a>
                    <a href="/products/semiconductor-components" class="products-module-item-3">
                        <figure class="products-module-img">
                            <img src="<?php bloginfo('template_url'); ?>/img/thumb-semiconductors-new.png" alt="Semiconductors">
                        </figure>
                        <h2 class="products-module-title arrowright-link">High-Purity</h2>
                    </a>
            </section>
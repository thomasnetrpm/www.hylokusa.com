

<section class="distributor-locator-module">
      <div class="inner-wrap">
        <div>
          <div class="dl-header">
            <h3 class="dl-us-header">Locate US Distributor by Zip Code:</h3>
          </div>
          <form action="http://www.hylokusa.com/" method="get" class="search-form dl-form">
            <div class="search-table">
              <div class="search-row">
                <div class="search-cell1">
                  <input type="text" id="search-site" value="" placeholder="Enter Zip Code" name="s" size="5" maxlength="5" title="Enter Zip Code">
                </div>
                <div class="search-cell2">
                  <input type="submit" value="Search">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
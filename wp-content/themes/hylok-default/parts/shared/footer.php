

</section>

<!--Site Footer-->
<section class="offers-module">
                <div class="inner-wrap">
                    
                    <div class="col-4 offers-trade-show">
                                        
                    <?php if(get_field('footer_trade_show_schedule','option')): ?>
<?php the_field('footer_trade_show_schedule', 'option'); ?>

<?php endif; ?>


                    
                    <!--<a href="#" class="col-4">
                    	<span class="btn">Browse Our Resources</span>
                    </a>-->
                    </div>
                    
                    <a href="http://www.hylokusa.com/assets/Fluid_Compatibility_Guide.pdf" rel="nofollow" class="col-4 offer-cta img-file">
                        <p><b>Download</b><br/>Fluid Compatibility Guide</p>
                    </a>
                    <a href="/certifications" rel="nofollow" class="col-4 col-last offer-cta img-ribbon">
                        <p><b>Download</b><br/>Industry Certifications</p>
                    </a>
                    
                    <!--<a href="#" class="col-4">
                        <h4>To better assist our customers, we've developed a few installation and maintenance guides. Additionally we hold a wide range of industry certifications.</h4>
                        <span class="btn">Browse Our Resources</span>
                    </a>-->
                    
                    
                                       
                </div>
            </section>
            
<footer class="site-footer" role="contentinfo">
    <div class="inner-wrap">
        <p>14211 Westfair West Drive, Houston, TX 77041, USA&emsp;•&emsp;Toll-Free: 888-300-5708&emsp;•&emsp;Fax: 832-634-2099&emsp;•&emsp;<a href="http://www.hylokusa.com/distributor-locator">Contact Us</a><br>
        &copy; <?php echo date("Y"); ?> Hy-Lok USA, Inc.&emsp;•&emsp;Website by <a href="http://business.thomasnet.com/rpm" target="_blank" rel="nofollow">ThomasNet RPM</a></p>
    </div>
</footer>
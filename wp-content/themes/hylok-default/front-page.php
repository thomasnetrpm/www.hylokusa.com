<?php

	/*
		Template Name: Front Page
	*/
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            <section class="site-intro">
                <!--<div class="inner-wrap">
                    <figure class="site-intro-img">
                        <img src="<?php bloginfo('template_url'); ?>/img/site-intro-product.png" alt="Hylok Products">
                    </figure>
                    <div class="site-intro-body">
                        <h1 class="site-intro-h1">
                            <span>Hy-Lok USA offers the highest quality instrument</span> <b>valves and fittings</b> for the most demanding fluid system <span>applications in North &amp; South America.</span>
                        </h1>
                        <div class="site-intro-cta-wrap">
                            <a href="http://www.hylokusa.com/locate-hylok-distributor" class="btn-outline-ico-map site-intro-cta-first">Locate a<br> Distributor</a>
                            <a href="http://valvesandfittings.hylokusa.com/" class="btn-outline-ico-cad site-intro-cta-last">Access<br> CAD Library</a>
                        </div>
                    </div>
                </div>-->
                <div class="inner-wrap">                    
                   <div class="site-intro-img">
                    <div class="flexslider product-carousel">
    <ul class="slides">
        <li>
            <figure class="">
                <img src="<?php bloginfo('template_url'); ?>/img/slide-ball-needle-valves.png" alt="Ball-Needle-Valves">
            </figure>
        </li>
        <li>
            <figure class="">
                <img src="<?php bloginfo('template_url'); ?>/img/slide-tube-fittings.png" alt="Tube-Fittings">
            </figure>
        </li>

        <li>
            <figure class="">
                <img src="<?php bloginfo('template_url'); ?>/img/slide-clean-fittings.png" alt="Clean-Fittings">
            </figure>
        </li>

        <li>
            <figure class="">
                <img src="<?php bloginfo('template_url'); ?>/img/slide-manifold-valves.png" alt="Manifold-Valves">
            </figure>
        </li>
        <li>
            <figure class="">
                <img src="<?php bloginfo('template_url'); ?>/img/slide-cng-products.png" alt="CNG-Products">
            </figure>
        </li>
    </ul>
</div>
</div>          <div class="site-intro-body">
                        <h1 class="site-intro-h1">
                            <span>Hy-Lok USA offers the highest quality instrument</span> <b>valves and fittings</b> for the most demanding fluid system <span>applications in North &amp; South America.</span>
                        </h1>
                        <div class="site-intro-cta-wrap">
                            <a href="/catalogs">
                            <span class="btn-outline-ico-catalogs site-intro-cta-first">Download <br> Product Catalogs</span>
                            </a>

                            <a href="/hy-lok-cad-drawings">
                            <span class="btn-outline-ico-cad site-intro-cta-last">Access<br> CAD Library</span>
                            </a>

                            <a href="/tube-fittings-performance-test-results">
                            <span class="btn-outline-ico-video site-intro-cta-third">Hy-Lok<br> Fitting Testing Results</span>
                            </a>
                        </div>
                    </div>
                </div>
                <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/distributor-locator-module' ) ); ?>

            </section>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/products-module','parts/shared/cad-module','parts/shared/resources-module', 'parts/shared/offers-module', 'parts/shared/form-module'  ) ); ?>

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
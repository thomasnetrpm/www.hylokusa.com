<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/page-intro' ) ); ?>

      <section class="site-content-container">
	    <div class="inner-wrap">

	        <article class="site-content-primary col-9"> 
	        	
	       		<?php the_content(); ?> 


				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>


				<?php if (is_page( '2198' )) : ?>
				<!--Sitemap Page-->
				<?php wp_nav_menu(array(
						'menu'            => 'Sitemap nav',
						'menu_class'      => 'sitemap',
						)); ?> 
					<?php endif; ?>                    
	        </article>
	        
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar','parts/shared/flexible-content-fullwidth' ) ); ?>
			
					 
			
		</div>
	</section>

<?php endwhile; ?>

<?php if(get_field('slide_cta') ): ?>
	 <p id="last"></p>
           <div id="slidebox"><a class="close">&nbsp;</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>
<?php endif; ?>


<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/resources-module','parts/shared/distributor-locator-module','parts/shared/footer','parts/shared/html-footer' ) ); ?>

<?php
/*
		Template Name: Page with Sidebar
	*/
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


<section class="site-intro">
  
    <div class="inner-wrap">
    		<h1 class="page-intro-header"><?php the_title(); ?></h1>
        <div class="site-intro-body">
        </div>
    </div>
</section>

      <section class="site-content-container">
	    <div class="inner-wrap">

	        <article class="site-content-primary col-9"> 
	        	
	       		<?php the_content(); ?> 
				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
				<?php if (is_page( '9' )) : ?>
					<!--Sitemap Page-->
				    <ul>
				    <?php
				    // Add pages you'd like to exclude in the exclude here
				    wp_list_pages(
				    array(
				    'exclude' => '',
				    'title_li' => '',
				    )
				    );
				    ?>
				    </ul>
				<?php endif; ?>                    
	        </article>
	        
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar','parts/shared/flexible-content-fullwidth' ) ); ?>

			

		</div>
	</section>

<?php endwhile; ?>

<?php if(get_field('slide_cta') ): ?>
	 <p id="last"></p>
           <div id="slidebox"><a class="close">&nbsp;</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>
<?php endif; ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/distributor-locator-module' ) ); ?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>